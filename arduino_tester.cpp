#include <Arduino.h>
#include <Wire.h>
#include "rgb_lcd.h"
#include <Stepper.h>

#define u8 uint8_t
#define DEBUG 1

rgb_lcd lcd;

const int stepsPerRevolution = 200;  // change this to fit the number of steps per revolution for your motor
// initialize the stepper library on pins 8 through 11:
// in1 -> 8
// in2 -> 9
// in3 -> 10
// in4 -> 11
Stepper myStepper(stepsPerRevolution, 8, 9, 10, 11);

int iPotiRaw;
int iStepperSpeed = 0;  // rpm

void setup()
{
	Serial.begin(115200);

    // set up the LCD's number of columns and rows:
    lcd.begin(16, 2);
    lcd.setRGB(255, 0, 0);

	myStepper.setSpeed(10);
    delay(1000);
}

void calcStepperSpeedFromPoti(int potiIn, int *speedOut, int minSpeed = 1, int maxSpeed = 60){
	float fTemp = minSpeed + potiIn / 1024.0f * (maxSpeed - minSpeed);
	*speedOut = (int)fTemp;
}

void loop()
{

	iPotiRaw = analogRead(0);
	calcStepperSpeedFromPoti(iPotiRaw, &iStepperSpeed, 1, 20);
	myStepper.setSpeed(iStepperSpeed);


    myStepper.step(66);
    delay(2000);

    myStepper.step(67);
    delay(2000);

    myStepper.step(67);
	delay(2000);
}
